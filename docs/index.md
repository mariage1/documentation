# Documentation : application de mariage 

???+ important "Documentation en cours d'écriture"
    Cette documentation n'est qu'informative. Elle n'est pas finie est peut-être amenée à changer. 
    Les pages avec :construction: dans le titre sont des pages qui peuvent-être amenée à disparaître.


## Introduction 

Cette application est développée pour aider à organiser les mariages. 

Une partie de ce projet est développée par **Antoine Berthier** dans le cadre du projet S4 du Master Informatique de la Faculté de Gestion, 
Economie et Sciences de l'Université Catholique de Lille.
Le code source du projet est disponible dans le groupe GitLab [mariage](https://gitlab.com/mariage1).