# Matiere utiliser  

| Cours      | Utilisation                          |
| :---------- | :----------------------------------- |
| Hybrid Mobile Developpement     |  Utilisation du framework Flutter pour la creation de l'interface |
| Cyber Security       | Concept de securité |
| Conception Logicielle Avancée  | Conception|
| Gestion de projet Agile         | Structure de la gestion de projet en  agilite  |
| UX Design | Conception de l'interface |
