# Suivi du Projet 

## Juin 

- Déploiement du projet. 

### API 

- Mise en place du service d'envoie de mail
- Websocket pour l'activation d'un utilisateur 
- Generation des invites
- Envoie de mail

### Interface 

- Page d'activation d'un utilisateur  
- Telechragement des invitations  

## Mai 2021

### Interface 

- Evolution sur la page profile
- Mise en place d'une animation de chargement

### API

- Mise en plce d'un deploiment sur heroku 
- Mise en place de test  

## Avril 2021

### Interface 

- Setting en fonction de l'environement  
- Mise en place du service d'auth et de role

### API

- Optimisation de la creation des invites par CSV
- Update des infos invites
- système pour avoir l'utilisateur courant

## Mars 2021

### Interface

- Mise en place de la page profil
- Travail sur les formulaire 
- Trie et evolution du tableau des invités  

### API

- Uplode des invites via fichier CSV
- Provisionement des Maries par defaut dans la BDD
- Mise en place du système de securité en token  

## Lundi 8 Mars 2021

### Tâche effectué

- Travail sur l'ORM  
- Documentation BDD  
- recherche sur les Tests  

## Mardi 24 Février 2021

### Tâches effectuées  

- Test Implementation d’OAuth2 
- Mise en place de postgresql


## Lundi 22 Février 2021

**Point avec Nicolas Baussart**

### Tâches effectuées  

- Continuation de la doc  
- Presentation du projet a Nicolas :  
  - Discution sur lhébergement : [clever-cloud](https://www.clever-cloud.com), [kimsufit](https://www.kimsufi.com/fr/)  
  - Choix de l'architecture API  
- Test sur l'intergration graphQL de fastAPI


### Choix durant cette scéance  

API avec une architecture graphQL

## Mardi 16 Février 2021

**Début du projet Master**  

### Tâches effectuées  

- Écriture d'un document de présentation du projet : [document PDF](../data/Presentation_project.pdf)
- Mise en place de la CI pour déployer la documentation.  
- Definition du workflow et des technologies utilisées.  






