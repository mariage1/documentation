# Gide Utilisateur : Invités

!!! info 
    Cette page explique l'utilisation de l'application pour les fonctions spécifiques aux Invités.

## Première connection

### Condition initial

- L'invité a reçu le faire-part avec l'invitation et don cle QR code.  

### Etapes

- Flashé le QR code pour accéder à la page de première connection  
    * S'il y a un problem de QR code une URL sera indiqué pour un accés manuellement à cette page.
- L'utilsateur est dirigé vers une page de premiere connection. Une adresse mail partielment obfusqué lui est proposé.
    - Si l'utilisateur acepte un mail avec un mdp passe sera envoyé a cette adresse
    - S'il refuse un mail sera envoyé a l'adresse de contacte pour prevenir qu'il a un probléme pour l'utilisateur.
- L'utilisateur pourra revenir a la page de connection et ce connecter avec le mot de passe envoyé par mail.  

## Page de profil

### Section Profil

La section profil resume les informations personel de l'utilisateur. Il peut aussi dire s'il vient ou pas grace au choix deroulant.

#### Modification du profil

Apres avoir activer le mode editable l'utilisateur peux changer ces infomations personel avant de valider ou non les modifications.

![update profile](../../data/user/profile_user_editable.png)

### Section famille 

De même que c'est information personel, un utilisateur peut dire si un membre de ca fammile sera present ou pas.  
Il peut aussi changer le nom de la famille.  

![update profile](../../data/user/profile_famille_editable.png)