# Gide Utilisateur

!!! info 
    Cette page explique l'utilisation de l'application pour les fonctions communes
    à l'ensemble des utilisateurs de l'application.


## Login 

Pour s'identifier un utilisateur doit renseigner son email et son mot de passe.  
Si l'utilisateur n'a pas activer son profil voire : [Premier connection](./gide-utilisateur-invités.md#Première connection)


![update profile](../../data/login.png)