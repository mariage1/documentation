# Gide Utilisateur : Mariés

!!! info 
    Cette page explique l'utilisation de l'application pour les fonctions spécifiques aux Mariés.


## Importer les invites depuis un fichier csv

### Description 

Une première liste d'inviter sera fait sur une feuille de calcule (Execl ... ).  
Ces invite sera alors exporter sous format csv.

### Structure du fichier

````
Id, first_name, laste_name, marie, categorie, importance
````

- `ID`: l'id est une information pour le fichier de base. Cette information n'est pas critique.
- `first_name` : Prénom de l'invite
- `laste_name` : Nom de l'invite
-  `marie` : Prénom du marier qui invite
-  `categorie` : Categorie de l'invite. Choix possible :
    -   Famille
    -   Amis
    -   Autre
- `importance` : Importance de l'invite. Choix possible :
    -   TI : Très Important
    -   I  : Important
    -   F  : Falcutatif 
    
### Utilisation 

![import](../../data/marier/import_csv.png)

### Step
-   Cliquer sur l'icone 
-   Choisir le fichier csv
-   Le traitement peux prendre un certain temps en fonction de la taille du fichier
-   Valider a la fin du chargement


## Ajouter un invite 

![import](../../data/marier/add_invite.png)

## Telechargé les invitations

![import](../../data/marier/qr_code_icone.png)

Lors de la demande de téléchargement il sera demandé si il faut généré les invitations.  
Cette operation peut prendre un certain temps.  
__Lors du premier téléchargement il faut faire l'operation__

![import](../../data/marier/qr_code.png)