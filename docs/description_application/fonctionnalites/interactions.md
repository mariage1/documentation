# Interactions

## :fire: Chat de discussion

Les utilisateurs de l'application devront avoir un lieu d'échange. Cet espace comportera des canaux 
de discussion. Des discussions bilatérales ou de groupe doivent être possibles. Tout le monde peut ajouter quelqu'un à la conversation.

Certains canaux pourront être privés pour que seules les personnes ajoutées puissent y accéder.
D'autres canaux seront semi-publics, à savoir visibles de tous les invités comme si le canal était public mais invisible aux mariés.
Des canaux seront entièrement publics. Cela leur permet d'être visible de tous et chacun peut le rejoindre librement *sans* avoir à envoyer une demande pour le rejoindre.

**Objectif :**  

- Créer une interaction en amont entre les invités.  
- Permettre aux invités de directement joindre les **Mariés**.  
- Permettre aux **Animateurs** de pouvoir échanger avec des invités sans que les Mariés puissent le voir.  

!!! info
    Seuls les **Administrateurs** pourront avoir accès à l'ensemble des canaux de discussion.
    Ils auront aussi un droit de modération sur les messages. 
!!! warning
    Si la mise en place de ce chat prend trop de temps ou est trop complexe, nous utiliserons Mattermost. Afin d'éviter de perdre un trop grand nombre d'utilisateurs, un lien mattermost sera joint à l'invitation papier et électronique ainsi qu'une notice d'utilisation dynamique, colorée et graphique pour permettre sa compréhension par tous. On pourrait imagine un bouton dynamique "rejoindre les discussions sur le mariage" intégré à la page dans laquelle les invités complèteront leurs informations personnelles. Penser à mettre une case "info" juste en dessous pour expliquer clairement et synthétiquement son utilité. Finir cette note "info" par un lien vers l'adresse mail des mariés pour tout complément d'information.

## :warning: Liste de cadeaux

L'application devra pouvoir lister une liste de souhaits de cadeaux que les **Mariés** auront séléctionnés.  

**Objectif :**   

- Informer les Invités du souhait des **Mariés**.  
- Permettre d'organiser la repartition de l'achat des cadeaux.


## :warning: Cagnotte  
En parallèle de la liste de cadeaux une cagnotte sera mise en place.

**Objectif :**   

- Permettre des dons plus petits qu'un cadeau.   
- Si certain ne trouvent pas leur bonheur dans la liste de cadeaux ou que tous les cadeaux de la liste ont déjà été sélectionnés.  

!!! warning 
    Voir comment intégrer un système de cagnotte dans l'application.
    Sinon se reporter sur une cagnotte en ligne.

## :question: Partage des images

Permettre aux utilisateurs de partager leurs photos de l'événement.

!!! info
    Voir aussi si on intègre avant le mariage une gallerie pour permettre au invités de mettre leurs meilleures photos des Mariés.
    *Faire un concours de la meilleure photo ?*

## :question: Vidéo final

Mettre en ligne un best of du mariage