# La gestion des Invités

## :fire: Enregistrement des Invités 
L'application doit pouvoir enregistrer une liste d'invités.

**Utilisateurs cibles :**  

- Les Mariés   

**Méthodes d'Enregistrement :**  

- Import d'un fichier csv  
- Saisie de formulaire

**Résultat final :**  

- La possibilité d'avoir la liste de l'ensemble des invités  
- Pouvoir récupérer 1 ou n invités en fonction de critères


## :fire: Génération des invitations 
L'application doit pouvoir générer un moyen de se connecter au site à partir d'un faire-part papier.
Ce moyen doit être utilisable par tous et comprendre des indications (ex : adresse mail des mariés) si les invités rencontrent des problèmes d'utilisation/de connexion. De plus des indications pour permettre de gérer un cas de défaillance
doivent y être indiqué. 

Ces invitations électroniques doivent pouvoir être envoyées avec/sur les faire-parts.

**Utilisateurs cibles :**  

- Les Mariés  

**Formes possibles d'invitation :**  

- Un QR code   
- Un lien  
- Les informations de contact d'un des Mariés en cas de pb. 

!!! warning
    Pour les informations de contact, prioriser le ou la marié(e) qui a invité cette personne.

**Resultat final :**  

- Fichier PDF avec l'ensemble des invitations

**Réception des invitations :**   

Lors de la réception du faire-part papier les **Invités** doivent pouvoir accéder à leur profil sur l'application, 
et le compléter si besoin (enfant(s), conjoins, adresse mail, etc). Attention, un Invité ne doit pas pouvoir ajouter un un autre Invité; il peut uniquement compléter les informations concernant sa famille.


## :fire: Confirmation ou Infirmation de sa présence

Un utilisateur doit pouvoir valider ou non sa présence au mariage.
La confirmation doit être validée avant la date fixée par les **Mariés**. Après cette date, contacter directement les mariés.
Une confirmation n'est pas définitive et peut être changée à tout moment (sauf après la date limite).

Si l'invitation est refusée certaines fonctionnalités (cadeau/cagnotte) doivent encore être accessibles.

**Utilisateur cible :**  

- Les Invités

## :fire: Modération des Invités   
Une modération doit être possible sur la liste des invités.

**Actions à prévoir**  

- Suppression de l'invité  
- Modification des informations  
- Renouvellement de l'invitation  
- Modification des identifiants  
- Passer l'utilisateur à un autre statut (admin, animateur ou invité)  

**Utilisateur cible :**  

- Les Mariés

## :warning: Statistiques et regroupements :

L'application doit pouvoir faire des regroupements sur la liste des invités en fonction de certaines caractéristiques.  
Ces regroupements devront pouvoir être sauvegardés et exportés pour être exploités en dehors de l'application.

*Exemple: pour la préparation des menus*

!!! warning "Information pour le logement"
    Les informations sur le logement sont importantes. Il faudra au moins mettre en place un moyen de savoir combien de personnes auront besoin d'un hôtel.    


**Résultat final :**  

- Fichier CSV ou PDF avec le résultat des regroupements  
- Sauvegarde de ces regroupements  
- Création de groupes

**Utilisateur cible :**  

- Les Mariés  
- Les Administrateurs (uniquement si dans le cadre d'un projet "surprise" inaccessible aux mariés)  
- Les Animateurs  

## :warning: Todo list

Mettre en place une Todo list dynamique pour les tâches de préparation à faire.

## :question: Vidéo d'accueil
Lors de la première connexion d'un utilisateur, une vidéo réalisée par les mariées doit pouvoir l'accueillir.

## :question: Génération d'un plan de table

L'application devra pouvoir générer un plan de table à partir de critères énoncés par les mariés. Il doit être ajustable manuellement après. L'objectif est de réduire une partie du casse-tête du plan de table sans toutefois priver les mariés de leur liberté de tout modifier comme ils le souhaitent.

