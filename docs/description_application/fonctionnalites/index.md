# Fonctionnalités


L'application sera faite autour de deux grands groupes de fonctionnalités :  
- La partie gestion des invités qui s'adresse aux Mariés.  
- La partie interaction qui sera un mini chat avec des fonctionnalités de partage de photos.

!!! info "Signalement de la priorité"
    Les noms des fonctionnalités seront accompagnés d'emojis pour signifier la priorité de cette dernière : 
    :fire: : Importante 
    :warning: : Moyenne 
    :question: : Basse  