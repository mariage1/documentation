# Profils Utilisateurs 

## Introduction

## Les Mariés 

Les Mariés sont les utilisateurs principaux de l'application. 
La majorité des fonctionnalités sont pensées pour leur profil. Notamment les fonctionnalités de gestion des invités et de 
maîtrise des coûts.

Ils devront avoir les pleins droits et pouvoir gérer les invités, certaines parties de l'application doivent pouvoir leur être 
cachées dans l'éventualité où les invités voudraient organiser des surprises.
Ce seront les seuls utilisateurs pour les fonctionnalités de gestion et d'organisation.

[Model Utilisateur](../technique/models.md#Utilisateur)

## Les Animateurs 

Ils seront des Invités avec des droits supplémentaires afin de réunir des groupes et organiser des surprises aux mariés s'ils le désirent. Ils auront un accès à certains endroits de l'application auxquels les mariés n'auront pas accès afin d'organiser les choses comme ils le souhaitent tout en gardant l'effet "surprise".

Les mariés doivent avoir le droit d'accorder et de retirer ce statut à un Invité. Les mariés peuvent également accorder le droit à un animateur d'ajouter et retirer le statut d'animateur à d'autres Invités. 

## Les Invités

Les Invités seront choisis par les mariés. Le profil des invités sera créé par les mariés qui feront une invitation pour qu'ils puissent se connecter et modifier leur profil si besoin.
L'application a pour but de créer une interaction entre les Invités.

## Les Administrateurs

Comme les mariés n'auront pas accès à certaines parties de l'application il faudrait un administrateur (infomaticien) pour gérer
ces parties-là (animées par les animateurs) en cas de problème technique. Ce rôle est uniquement préventif et concerne un périmètre restreint de l'application. 