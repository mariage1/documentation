# La base de données

## Model de données 

![model](../data/postgres_colection.png)

## Postgre

Variable d'environnement :

- `DB_SERVER_NAME` : Host du serveur posgreSQL  
- `DB_USER` : User de la bdd  
- `DB_PASSWORD` : Password du user  
- `DB_NAME` : Non de la bd   


### Configuration docker-compose

image posgres utiliser : `postgres`

Informations : 

- Utilisateur : postgre  
- Password : postgre 
- Base de donées : postgre


### ORM : SQLAlchimy

version : `1.4.0b3` (version beta)  
doc : [v1.4](https://docs.sqlalchemy.org/en/14/)  

fichier des models : `app/models`

| Table      | Classe                          | Description |
| :---------- | :----------------------------------- | :---------------|
| `maries`       |  `Marie` | Information des mariés |
| `invites`       | `Invite` |Information des invite |
| `administreteur`    | `Administrateur` | Information des administrateurs |
| `famille`           | `Fammile`  | famille qui regroupe plusieurs invités |
