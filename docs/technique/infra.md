# Infra du projet

## Backend 

### API 

**Roles** :

- Gestion des Authentifications  
- Permet de faire et télécharger les invitations  
- Permet de gérer un ou des invités
- Permet de renvoyer le liste des invités
- Gére le chat du mariage  

**Déploiement**

Déploiement sur Heroku lors de la pose d'un tage `V*.*.*-heroku`

### BDD Postgre :

**Roles** :  

- Stocker les informations des utilisateurs  
- Stocker les informations du chat  


## Interface

L'interface sera fait avec le Framework [Flutter](https://flutter.dev/) 

