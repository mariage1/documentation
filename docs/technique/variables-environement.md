# Variables d'environement

## Variable application python

| Nom      | Role                         | required | default value | info |
| :---------- | :----------------------------------- | :--------------- | :--- | :--- |
| `ENV_NAME`       |  Nom de l'environment | `false` | `dev` |
| `DATABASE_URL`       | URL du serveur postgres |`false` | `postgres` | Si cette variable est defini, les autres variable `DB_*` ne sont pas a definir |
| `DB_USER`    | Nom d'utilisateur pour l'utilisation  | `false` | `postgres` |
| `DB_PASSWORD`           | Mot de passe de l'utilisateur postgres  | `false` | `postgres` |
| `DB_SERVER_NAME`           | Nom du server postgres  | `false` | `localhost` |
| `DB_NAME`           | Nom de la basse de donée  | `false` | `postgres` |
| `SECRET_KEY`           | clé secret pour les actions de chiffrement  | `true` | `your-secret-key` | A modifier obligatoirement en cas de deploiment |
| `TOKEN_JWT_ALGORITHM`           | Algorithm pour la creation du token |`true` | `HS256` |
| `TOKEN_JWT_EXPIRE`           | Nombre d'heure de la validiter du token | `true` | 5 |
| `CLIENT_URL`           | URL de l'interface flutter  | `true` | `http://localhost/` |
| `DEFAULT_MARIER_PASSWORD`           | Mot de passe par defaut des marier  | `true` | `your-secret-password` | A modifier obligatoirement en cas de deploiment |
| `GMAIL_USER`           | User du compte gmail pour l'envoi d'un mail   | `true` |
| `GMAIL_PASSWORD`           | Password du compte gmail pour l'envoi d'un mail  | `true` |
| `DEBUG_LEVEL`           | Niveau de debug pour les logs de l'application  | `true` | `info` |
| `DEFAULT_MARIER_EMAIL_ONE`         | email pour le compte marie 1  | `true` | | Email d'initialisation du premier marier |
| `DEFAULT_MARIER_EMAIL_TWO`           | email pour le compte marie 2 | `true` | | Email d'initialisation du second marier |

