# Documentation de l'API 

L'API sera développé avec le framework [FastAPI](https://fastapi.tiangolo.com/). 

## Architecture : 

### GraphQL 

La majorité des opérations sera fait via une architecture GraphQL. Elle permettra de facilité la gestion de mes objets 
et des liens entre les objets. 

### REST

GraphQL ne prend pas la gestion de fichier dans ces specs. Toutes les operations 
sur un ou des fichiers sera donc effectuer via une route REST.

## Sécurité

Pour la sécurité j'implémenterai OAth2 par Bearer en JWT token. [Doc fastAPI](https://fastapi.tiangolo.com/tutorial/security/oauth2-jwt/)

