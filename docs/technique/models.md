# Models 

## Specification des models

### Utilisateur 

#### Description

Les **Utilisateurs** sont des personnes définie par :    

- Leur nom  
- Leur prénom  
- Un mail de contact  
- Un mdp  

L'application devra sauvegarder en plus :  

- Un identifiant unique  
- Le moment de la création du profil  
- Si le profile est activé  
- La dernière connection du profil  

L'application à 3 types d'utilisateurs :  

- Les Mariés  
- Les Administrateur  
- Les Invités  
  

##### Les Mariés 

Les **Mariés** sont deux **Utilisateur**. 
Ils auront aces à l'ensemble des fonctionnalités de l'application. Cependant certains canaux de discussion leur seront restraint.
Ils auront des droits de moderation dans les discutions et sur le profile de l'ensemble des utilisateurs.
Ils auront un droit de moderation sur les invités et définiront les **Administrateurs** et les **Animateurs**.

##### Les Administrateurs 

Les **Administrateurs** seront des profils specials crée par les **Mariés**. 
Ils auront les mêmes droits d'administration que les **Mariés** mais contrairement à eux ils auront aces à l'ensemble 
des conversations. Les **Administrateurs** ne sont pas des **Invités** et ne figureront pas dans la liste des invités.

##### Les Invités

Les Invités seront des profils crée par les mariés. En plus des informations de tous les utilisateurs Ils auront les informations suivantes :  

- Le lien avec les **Mariés** (invité par quel marié ou par les parents)  
- Catégorie d'invité (Famille ou Amis)  
- Les membres de leur familles proche   
- S'il a confirmer ca venu.   

Certain **Invités** auront aussi le rôle de **Animateurs**. Ces **Invités** pourront aussi crée des canaux de discution 
privé qui ne seront pas accessible par un ou les **Mariés**

#### Diagramme de classe des utilisateur de l'application

!!! warning
    Cette representation n'est pas définitif. De plus c'est le model, à ne pas confondre le schema bdd.

````mermaid
classDiagram

class Utilisateur{
    - uuid id
    - str last_name
    - str first_name
    - str mail
    - str hashed_password
    - DateTime datetime_create_user
    - DateTime datetime_last_action
    - bool is_active
}

class Invités {
    - Mariés liens
    - Categorie categorie
    - Invités Famille
    - bool has_validate
}


class Categorie
<<enum>> Categorie

Mariés --|> Utilisateur


Invités --|> Utilisateur
Invités *-- Mariés
Invités *-- Categorie
Invités --o Invités

Animateurs --|> Invités

Administrateurs --|> Utilisateur

````

### Conversation 

Pour la partie interactration il serra mis en place un systéme de chat entre les participents.
Ce chat reprendra un model calssique de conversation, message et reponse.  

!!! warning
    Dans un premier temps les message ne seront que textuel. En fonction de l'avancement du projet on pourra faire 
    l'integration de gif, vidéo ou image.  
    Il n'est pas n'écesaire de mettre en place un systéme de notification dans l'imédiat.

#### Les caneaux de discution 

Les caneaux de discution seront le lieu ou les discution se feront.  
Il y aura deux type de canaux :  

- Public : Où tous les membres qui veulent peut s'y rendre.  
- Privé  : Où seul les membres ajouter peuvent s'y rendre.  

Les canaux privret avec seulment deux personne seront clasifier comme discution priver.  
Seul les **Utilisateur** **Administrateur** pouront voire l'ensemble des canaux.

Un canal a un nom est peux avoir un theme.

#### Message

Dans les canaux les **Utilisateur** pouront poster des messages. Chaque message sera lier à sont auteur. 
Les messages qui répond a un précédant message auront un message pére.


````mermaid
classDiagram
    class Canal {
        uuid id
        Message messages
        timestamp create_time
        string theme
        String nom
        bool is_private
    }
    
    class Message {
        uuid id
        string message
        Utilisateur author
        Message message_pere
        timestamp create_time
    }
    
    Canal *-- Message
    Message --o Message
    
    Utilisateur --o Message
````
