# Organisation du GitLab

Lien vers l'organisation GitLab : [mariage](https://gitlab.com/mariage1)  


## Documentation 

Depot [documentation](https://gitlab.com/mariage1/documentation)


## Utilisation de la CI

Pour la CI j'utiliserais les GitLab runner.  
Comma je suis seul je dois mettre en place une grosse couverture pour assurer la qualité de code.

Build des image docker.

## Les depots 

Il y auras 4 depots principale :  
- Pour la documentation  
- Pour le back-end (api fastAPI)  
- Pour le front  
- Pour l'infrastructure  

## Convention 

Les conventions git seront appliqué sur tous les depot a l'exception de la documentation.

### GitFlow 

Branche principale : `Master`. 

Relise des versions via tag.

Nommage des branche et des commits : [Conventional Commits](https://www.conventionalcommits.org/fr/v1.0.0-beta.3/)

